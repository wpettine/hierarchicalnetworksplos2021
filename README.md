This repository contains python code used in the paper:
 
 Pettine, W. W., Louie, K., Murray, J. D, Wang, X. J., "Hierarchical Network Model Excitatory-Inhibitory Tone Shapes Alternative Strategies for 
Different Degrees of Uncertainty in Multi-Attribute Decisions." PLoS Computational Biology, 2021.

In addition to packages that perform the simulations and plot the results, we have included a jupyter lab notebook, 'figures.ipynb' that reproduces the main figures from the paper. The larger simulations were run on a cluster. If you would like guidance implementing these code packages on a cluster, please contact us. 

The models were written using python 3.7. See the requirements.txt file for a list of required packages. 

If you have any questions, please contact warren.pettine@gmail.com. 